#!/usr/bin/env python

import asyncio
import io
import sqlite3
from concurrent.futures import ThreadPoolExecutor

import boto
import exifread
from boto.s3.bucketlistresultset import BucketListResultSet
from boto.s3.key import Key


class DBWriter:
    def __init__(self, filename="waldo.db"):
        self.conn = sqlite3.connect(filename)
        self.cur = self.conn.cursor()

        self._create_tables()

    def _create_tables(self):
        """Initialize the tables in the database."""
        self.cur.executescript("""
        BEGIN;
        CREATE TABLE IF NOT EXISTS "main"."image_file" (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            "filename" TEXT NOT NULL,
            UNIQUE("filename")
        );
        CREATE INDEX IF NOT EXISTS image_file_filename ON image_file(filename);

        CREATE TABLE IF NOT EXISTS "main"."exif_tag" (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            "file_id" INTEGER REFERENCES image_file(id),
            "name" TEXT NOT NULL,
            "value" TEXT NOT NULL,
            UNIQUE("file_id", "name") ON CONFLICT REPLACE
        );

        CREATE INDEX IF NOT EXISTS image_exif_tag_file_id ON exif_tag(file_id);
        CREATE INDEX IF NOT EXISTS image_exif_tag_file_id_name ON exif_tag(file_id, name);

        COMMIT;
        """)
        self.commit()

    def create_file(self, filename):
        """Create a file row in the database (unless it exists) and return its id."""

        # Check if the filename already exists, and return its ID if so.
        self.cur.execute("SELECT id FROM image_file WHERE filename = ?", (filename,))
        file_id = self.cur.fetchone()
        if file_id:
            return file_id[0]

        # If it doesn't exist, create it.
        self.cur.execute("INSERT INTO image_file (filename) VALUES (?)", (filename,))
        self.cur.execute("SELECT last_insert_rowid()")
        return self.cur.fetchone()[0]

    def create_tag(self, file_id, name, value):
        """Create an EXIF tag in the database for a given file."""
        self.cur.execute("""
        INSERT INTO exif_tag (file_id, name, value) VALUES (?, ?, ?)
        """, (file_id, name, value))
        self.cur.execute("SELECT last_insert_rowid()")
        return self.cur.fetchone()[0]

    def commit(self):
        """Commit the changes in the database to disk."""
        self.conn.commit()


#######
# EXIF methods

def get_exif_offset(header: bytes):
    """
    Get the EXIF offset from a JPEG header.

    Returns the endianness of the file, an offset and whether a fake EXIF
    header is needed. The purpose of this function is mainly to return the
    offset of the EXIF chunk so it can later be retrieved efficiently.

    Raises ValueError if there is an error.
    """
    f = io.BytesIO(header)
    endian, offset, fake_exif = exifread.get_exif_location(f)
    return endian, offset, fake_exif


def get_exif_tags(content: bytes, endian: str, fake_exif: int):
    """
    Read the EXIF tags from a (max) 64-KB chunk.

    `content` must be a bytes object that starts at the beginning of the EXIF
    data, but may extend past it.

    Returns a dictionary of keys.
    """
    f = io.BytesIO(content)
    tags = exifread.process_file(
        f,
        endian=endian,
        offset=0,
        fake_exif=fake_exif
    )
    return tags


#######
# S3 methods
#

def get_exif_data_from_object(obj: Key):
    """
    Retrieve the EXIF data from an S3 object.

    This function only fetches the parts of the file that will be needed to
    retrieve the EXIF data. It does this in two calls, as we need one call to
    discover the EXIF chunk location and one to read the latter.
    """
    header = obj.get_contents_as_string(headers={'Range': 'bytes=0-10240'})
    endian, offset, fake_exif = get_exif_offset(header)
    # XXX: Here, we could optimize further and use the first chunk if the EXIF
    # data is in the first 10 KB.
    exif = obj.get_contents_as_string(
        headers={'Range': 'bytes=%s-%s' % (offset, offset + 64 * 1024)}
    )
    tags = get_exif_tags(exif, endian, fake_exif)
    return tags


def get_exif(obj: Key):
    """
    Retrieve the EXIF data from an S3 object, handling errors in the process.

    Returns a 2-tuple of (exif_dict, obj) if successful or (error_str, None)
    if not.
    """
    print("\x1b[1;32;40mWorker:\x1b[0m Reading EXIF data for %s..." % obj.name)
    try:
        return get_exif_data_from_object(obj), obj
    except ValueError:
        return "There was an error parsing data for %s." % obj.name, None
    except boto.exception.S3ResponseError:
        return "Could not access %s." % obj.name, None


def get_bucket_keys(bucket_name: str) -> BucketListResultSet:
    """
    Return all the keys in an S3 bucket.
    """
    print("\x1b[1;32;40mPre:\x1b[0m Listing keys in bucket...")
    conn = boto.connect_s3()
    bucket = conn.get_bucket(bucket_name)
    return bucket.list()


#######
# Orchestration
#

async def main(max_workers=20):
    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        # Put all the futures in a list, to prepare them for execution.
        loop = asyncio.get_event_loop()
        futures = []

        # Since these are getting loaded in memory, this probably won't scale
        # beyond a few thousand objects, but I'm not sure of the desired scale,
        # so this seems good enough for now.
        for key in get_bucket_keys("waldo-recruiting"):
            # Don't bother with files that don't end in .jpg.
            if not key.name.lower().endswith(".jpg"):
                continue

            print("\x1b[1;32;40mMain:\x1b[0m Adding %s to the queue..." % key.name)
            future = loop.run_in_executor(
                executor,
                get_exif,
                key
            )
            futures.append(future)

    writer = DBWriter()

    # Wait until all the coroutines are done and collate the results.
    # XXX: An obvious optimization here is to pipeline this so each fetching
    # coroutine sends to one writing coroutine, so we don't have to wait for all
    # the files before we write the results.
    for result, obj in await asyncio.gather(*futures):
        if not obj:
            print("\x1b[1;31;40mWarning:\x1b[0m %s" % result)
            continue

        print("\x1b[1;32;40mWriting:\x1b[0m Persisting tags for %s..." % obj.name)
        file_id = writer.create_file(obj.name)
        for name, value in result.items():
            # I don't know how the reader wants the data, but repr() preserves
            # the most data, so I'm using that.  -- Stavros
            writer.create_tag(file_id, name, repr(value))

        writer.commit()

    print("\x1b[1;34;40mAll done.\x1b[0m")


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
