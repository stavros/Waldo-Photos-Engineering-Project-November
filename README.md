Waldo Engineering Project
=========================

This repository contains the scripts for the Waldo Photos Engineering Project.

To use, just run `./waldo.py`. Almost everything is contained in that script,
although the `exifread` library was vendored in and slightly changed to allow
for more efficient EXIF data fetching.

To run:

~~~.python
$ pip install -Ur requirements.txt
$ ./waldo.py
~~~

- Stavros
