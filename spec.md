# Waldo Photos Engineering Project

We believe the best method to assess an engineer is by writing software to solve
a problem and then talking about that software in a collaborative environment.
The goal of this project is to produce a working system that can be used as
a conversation piece during your on-site interview. Be prepared to:

* Present your solution to a group of smart engineers like yourself.
* Talk about the decisions that went into the creation of your solution.
* Explain how you see the solution evolving over time.
* Discuss the runtime characteristics of the system.

## Environment

We have a screen that can be used to present the code using WIFI + Airplay or
HDMI. If you are not able to provide your own laptop you may borrow an OSX
laptop.

## Problem Statement

Using any language and data-store of your choice, write an application that
reads a set of photos from a network store (S3), parses the EXIF data from the
photos and indexes the EXIF key/value pairs into a query-able store by unique
photo.

## Deliverables:

1. Source code tracked in a public git repository.

## Tips:

* Don't spend more than a few hours on the project. We are looking for a strong
  understanding of the key concepts of implementing a data-flow system, not
  a perfect implementation.
* The main areas we will be evaluating are organization of responsibility,
  concurrency composition, overall performance and resilience to failures.
* Have fun and keep in mind not all aspects of the evaluation need to be
  demonstrated in code. Explaining understanding via comments, verbal
  communication, and/or whiteboard can be effective for the purpose of this
  project.

## Resources

* S3 Bucket (input): http://s3.amazonaws.com/waldo-recruiting
